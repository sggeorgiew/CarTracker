﻿using CarTracker.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;

namespace CarTracker.Helpers
{
    public class JsonFormat<T> where T : IModel
    {
        public List<T> Data { get; set; }

        private JsonFormat()
        { }

        private JsonFormat(List<T> items)
        {
            this.Data = items;
        }

        static JsonFormat()
        {
            if (!Directory.Exists(Constants.DirectoryPath))
            {
                Directory.CreateDirectory(Constants.DirectoryPath);
            }
        }

        public static JsonFormat<T> Import(string jsonFile)
        {
            string json;

            using (FileStream fs = new FileStream(jsonFile, FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    json = sr.ReadToEnd();
                }
            }

            var result = JsonConvert.DeserializeObject<JsonFormat<T>>(json);

            if (result != null)
            {
                return result;
            }

            return new JsonFormat<T>(new List<T>());
        }

        public static void Export(string jsonFile, List<T> items)
        {
            using (FileStream fs = new FileStream(jsonFile, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(new JsonFormat<T>(items)));
                }
            }
        }
    }
}
