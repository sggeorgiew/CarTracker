﻿using System;

namespace CarTracker.Helpers
{
    public static class Constants
    {
        public const string ConfigFileName = "config.json";

        public const string DirectoryPath = "./Data/";
        public const string VehicleDataFilePath = DirectoryPath + "vehicle-data.json";
    }
}
