﻿using CarTracker.Tools;
using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarTracker.Views
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        private List<Accent> accentColors { get; set; }
        private List<AppTheme> appThemes { get; set; }

        private Accent selectedAccent { get; set; } = ThemeManager.GetAccent(Config.Settings.AppSettings.Accent);
        private AppTheme selectedTheme { get; set; } = ThemeManager.GetAppTheme(Config.Settings.AppSettings.Theme);

        public SettingsWindow()
        {
            InitializeComponent();

            Loaded += WindowLoaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            accentColors = ThemeManager.Accents.ToList();
            AccentsComboBox.ItemsSource = accentColors.Select(x => x.Name);

            appThemes = ThemeManager.AppThemes.ToList();
            ThemesComboBox.ItemsSource = appThemes.Select(x => x.Name);

            AccentsComboBox.SelectedIndex = accentColors.FindIndex(a => a == selectedAccent);
            ThemesComboBox.SelectedIndex = appThemes.FindIndex(t => t == selectedTheme);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Config.Settings.AppSettings.Accent = selectedAccent.Name;
                Config.Settings.AppSettings.Theme = selectedTheme.Name;
                Config.Save();

                ApplicationStyleChanger.ChangeApplicationStyle(selectedAccent, selectedTheme);

                ResultLabel.Content = "Successfully saved!";
            }
            catch
            {
                ResultLabel.Content = "Something went wrong!";
            }
        }

        private void AccentsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedAccent = accentColors[(sender as ComboBox).SelectedIndex];
            ApplicationStyleChanger.ChangeWindowStyle(this, selectedAccent, selectedTheme);
        }

        private void ThemesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedTheme = appThemes[(sender as ComboBox).SelectedIndex];
            ApplicationStyleChanger.ChangeWindowStyle(this, selectedAccent, selectedTheme);
        }
    }
}
