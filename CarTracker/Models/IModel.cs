﻿using System;

namespace CarTracker.Models
{
    public interface IModel
    {
        int Id { get; set; }
    }
}
