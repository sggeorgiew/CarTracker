﻿using System;

namespace CarTracker.Models
{
    public class ApplicationSettings
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public string Accent { get; set; }
        public string Theme { get; set; }
    }
}
